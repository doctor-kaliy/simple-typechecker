name := "simple-typechecker"

version := "0.1"

scalaVersion := "2.13.1"

libraryDependencies +=
  "org.typelevel" %% "cats-core" % "2.1.0"
libraryDependencies += "org.typelevel" %% "cats-mtl" % "1.1.1"
addCompilerPlugin("org.typelevel" % "kind-projector" % "0.13.2" cross CrossVersion.full)
scalacOptions ++= Seq(
  "-Xfatal-warnings"
)
libraryDependencies += "org.antlr" % "antlr4-runtime" % "4.9.3"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.3.0-SNAP2" % Test

