import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import typechecker.{Expression, Type, TypeChecker}

import scala.language.implicitConversions

class Test extends AnyFlatSpec with should.Matchers {
  implicit def strToExp(s: String): Expression = utils.Parser.parseExpression(s)
  implicit def strToType(s: String): Type = utils.Parser.parseType(s)
  def check(
    e: Expression,
    context: typechecker.TypeChecker.Context = Map()
  ): Either[String, Type] = TypeChecker.run(e, context)

  it should "typecheck with a -> a" in {
    check("\\x:a. x") shouldBe Right(strToType("a -> a"))
  }

  it should "typecheck with (a -> b) -> a -> b" in {
    check("\\x:a -> b. \\y : a. x y") shouldBe Right(strToType("(a -> b) -> a -> b"))
  }

  it should "fail with string message" in {
    assert(check("\\x:a -> b. \\y : c. x y").isLeft)
  }

  private val natContext: Map[String, Type] = Map(
    "succ" -> "Nat -> Nat",
    "zero" -> "Nat"
  )

  it should "typecheck with Nat(increment)" in {
    check("\\x : Nat. succ x", natContext) shouldBe Right(strToType("Nat -> Nat"))
  }

  it should "typecheck with Nat(2 is Nat)" in {
    check("succ (succ zero)", natContext) shouldBe Right(strToType("Nat"))
  }

  it should "fail because of missing variable in context" in {
    assert(check("\\x: Nat.\\y: Nat.\\z: Nat -> Nat -> Nat. hello z x y", natContext).isLeft)
  }

  private val natHelloTest = "\\x: Nat.\\y: Nat.\\z: Nat -> Nat -> Nat. hello z x y"
  it should s"$natHelloTest fail because of wrong type in context" in {
    assert(check(
      natHelloTest,
      natContext.updated("hello", "Nat -> Nat -> Nat -> Nat")).isLeft)
  }

  private val appAndAbs = "(\\x:Boolean.t0) g"
  private val appAndAbsC: Map[String, Type] = Map(
    "t0" -> "Int",
    "g" -> "String"
  )
  it should s"$appAndAbs fail because of wrong type in context" in {
    assert(check(
      appAndAbs,
      appAndAbsC).isLeft)
  }

  it should "fail because of 2 occurrences of the same variable" in {
    assert(check("(\\x:b.x)", Map("x" -> "b -> b")).isLeft)
  }
}

