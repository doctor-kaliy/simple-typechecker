import typechecker.{Abstraction, EApplication, EVariable, TVariable, TypeChecker}
import utils.Parser

import scala.io.StdIn.readLine

object Main extends App {
  val raw = readLine().split("#").toList
  if (raw.length != 2) {
    println(raw)
    println("Invalid data format. Expected variable1 : type1, variable2 : type2, ... # expr")
  } else {
    val (rawContext :: rawExpr :: _) = raw
    val rawVariables = rawContext.split(",").toList.map(_.split(":"))
    if (!rawVariables.forall(_.length == 2)) {
      println("Invalid data format. Expected variable : type")
    } else {
      val contextSize = rawVariables.size
      val context = rawVariables.map(xs => (xs(0).trim, Parser.parseType(xs(1)))).toMap
      if (context.size != contextSize) {
        println("Context must contain only unique variables")
      } else {
        TypeChecker.run(Parser.parseExpression(rawExpr), context) match {
          case Left(value) => println(s"Error: $value")
          case Right(value) => println(s"Success: type is $value")
        }
      }
    }
  }
}
