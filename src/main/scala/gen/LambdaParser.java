// Generated from /home/eugene/pidor/simple-typechecker/src/main/scala/Lambda.g4 by ANTLR 4.9.2

package gen;
import typechecker.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LambdaParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		SKIP_WHITESPACES=1, ARROW=2, LPAR=3, RPAR=4, NAME=5, COLON=6, LAMBDA=7, 
		DOT=8;
	public static final int
		RULE_lambda = 0, RULE_pexpr = 1, RULE_expr = 2, RULE_ptype = 3, RULE_type = 4;
	private static String[] makeRuleNames() {
		return new String[] {
			"lambda", "pexpr", "expr", "ptype", "type"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, "'->'", "'('", "')'", null, "':'", "'\\'", "'.'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "SKIP_WHITESPACES", "ARROW", "LPAR", "RPAR", "NAME", "COLON", "LAMBDA", 
			"DOT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Lambda.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public LambdaParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class LambdaContext extends ParserRuleContext {
		public List<typechecker.Expression> exps;
		public Token nm;
		public TypeContext t;
		public ExprContext e;
		public TerminalNode LAMBDA() { return getToken(LambdaParser.LAMBDA, 0); }
		public TerminalNode COLON() { return getToken(LambdaParser.COLON, 0); }
		public TerminalNode DOT() { return getToken(LambdaParser.DOT, 0); }
		public TerminalNode NAME() { return getToken(LambdaParser.NAME, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public LambdaContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public LambdaContext(ParserRuleContext parent, int invokingState, List<typechecker.Expression> exps) {
			super(parent, invokingState);
			this.exps = exps;
		}
		@Override public int getRuleIndex() { return RULE_lambda; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LambdaListener ) ((LambdaListener)listener).enterLambda(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LambdaListener ) ((LambdaListener)listener).exitLambda(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LambdaVisitor ) return ((LambdaVisitor<? extends T>)visitor).visitLambda(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LambdaContext lambda(List<typechecker.Expression> exps) throws RecognitionException {
		LambdaContext _localctx = new LambdaContext(_ctx, getState(), exps);
		enterRule(_localctx, 0, RULE_lambda);
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(10);
			match(LAMBDA);
			setState(11);
			((LambdaContext)_localctx).nm = match(NAME);
			setState(12);
			match(COLON);
			setState(13);
			((LambdaContext)_localctx).t = type();
			setState(14);
			match(DOT);
			setState(15);
			((LambdaContext)_localctx).e = expr();
			_localctx.exps.add(new typechecker.Abstraction((((LambdaContext)_localctx).nm!=null?((LambdaContext)_localctx).nm.getText():null), ((LambdaContext)_localctx).t.tp, ((LambdaContext)_localctx).e.exp));
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PexprContext extends ParserRuleContext {
		public List<typechecker.Expression> exps;
		public ExprContext e;
		public Token nm;
		public TerminalNode LPAR() { return getToken(LambdaParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(LambdaParser.RPAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode NAME() { return getToken(LambdaParser.NAME, 0); }
		public PexprContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PexprContext(ParserRuleContext parent, int invokingState, List<typechecker.Expression> exps) {
			super(parent, invokingState);
			this.exps = exps;
		}
		@Override public int getRuleIndex() { return RULE_pexpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LambdaListener ) ((LambdaListener)listener).enterPexpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LambdaListener ) ((LambdaListener)listener).exitPexpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LambdaVisitor ) return ((LambdaVisitor<? extends T>)visitor).visitPexpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PexprContext pexpr(List<typechecker.Expression> exps) throws RecognitionException {
		PexprContext _localctx = new PexprContext(_ctx, getState(), exps);
		enterRule(_localctx, 2, RULE_pexpr);
		try {
			setState(25);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LPAR:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(18);
				match(LPAR);
				setState(19);
				((PexprContext)_localctx).e = expr();
				setState(20);
				match(RPAR);
				_localctx.exps.add(((PexprContext)_localctx).e.exp);
				}
				}
				break;
			case NAME:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(23);
				((PexprContext)_localctx).nm = match(NAME);
				_localctx.exps.add(new EVariable((((PexprContext)_localctx).nm!=null?((PexprContext)_localctx).nm.getText():null)));
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public typechecker.Expression exp;
		public LambdaContext lambda() {
			return getRuleContext(LambdaContext.class,0);
		}
		public List<PexprContext> pexpr() {
			return getRuleContexts(PexprContext.class);
		}
		public PexprContext pexpr(int i) {
			return getRuleContext(PexprContext.class,i);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LambdaListener ) ((LambdaListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LambdaListener ) ((LambdaListener)listener).exitExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LambdaVisitor ) return ((LambdaVisitor<? extends T>)visitor).visitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		ExprContext _localctx = new ExprContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_expr);

		        List<typechecker.Expression> exps = new ArrayList<>();
		    
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(42);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				{
				{
				setState(30);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==LPAR || _la==NAME) {
					{
					{
					setState(27);
					pexpr(exps);
					}
					}
					setState(32);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(33);
				lambda(exps);
				}
				}
				break;
			case 2:
				{
				{
				setState(35); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(34);
					pexpr(exps);
					}
					}
					setState(37); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==LPAR || _la==NAME );
				setState(40);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LAMBDA) {
					{
					setState(39);
					lambda(exps);
					}
				}

				}
				}
				break;
			}

			        ((ExprContext)_localctx).exp =  exps.stream().reduce((x, y) -> new EApplication(x, y)).get();
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PtypeContext extends ParserRuleContext {
		public typechecker.Type tp;
		public Token n;
		public TypeContext t;
		public TerminalNode NAME() { return getToken(LambdaParser.NAME, 0); }
		public TerminalNode LPAR() { return getToken(LambdaParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(LambdaParser.RPAR, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public PtypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ptype; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LambdaListener ) ((LambdaListener)listener).enterPtype(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LambdaListener ) ((LambdaListener)listener).exitPtype(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LambdaVisitor ) return ((LambdaVisitor<? extends T>)visitor).visitPtype(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PtypeContext ptype() throws RecognitionException {
		PtypeContext _localctx = new PtypeContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_ptype);
		try {
			setState(53);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NAME:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(46);
				((PtypeContext)_localctx).n = match(NAME);
				((PtypeContext)_localctx).tp =  new typechecker.TVariable((((PtypeContext)_localctx).n!=null?((PtypeContext)_localctx).n.getText():null));
				}
				}
				break;
			case LPAR:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(48);
				match(LPAR);
				setState(49);
				((PtypeContext)_localctx).t = type();
				setState(50);
				match(RPAR);
				((PtypeContext)_localctx).tp =  ((PtypeContext)_localctx).t.tp;
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public typechecker.Type tp;
		public PtypeContext pt;
		public PtypeContext t1;
		public TypeContext t2;
		public PtypeContext ptype() {
			return getRuleContext(PtypeContext.class,0);
		}
		public TerminalNode ARROW() { return getToken(LambdaParser.ARROW, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LambdaListener ) ((LambdaListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LambdaListener ) ((LambdaListener)listener).exitType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LambdaVisitor ) return ((LambdaVisitor<? extends T>)visitor).visitType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_type);
		try {
			setState(63);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(55);
				((TypeContext)_localctx).pt = ptype();
				((TypeContext)_localctx).tp =  ((TypeContext)_localctx).pt.tp;
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(58);
				((TypeContext)_localctx).t1 = ptype();
				setState(59);
				match(ARROW);
				setState(60);
				((TypeContext)_localctx).t2 = type();
				((TypeContext)_localctx).tp =  typechecker.Type.arrow(((TypeContext)_localctx).t1.tp, ((TypeContext)_localctx).t2.tp);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\nD\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\5\3\34\n\3\3\4\7\4\37\n\4\f\4\16\4\"\13\4\3\4\3\4\6\4"+
		"&\n\4\r\4\16\4\'\3\4\5\4+\n\4\5\4-\n\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\5\58\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6B\n\6\3\6\2\2\7\2\4\6"+
		"\b\n\2\2\2E\2\f\3\2\2\2\4\33\3\2\2\2\6,\3\2\2\2\b\67\3\2\2\2\nA\3\2\2"+
		"\2\f\r\7\t\2\2\r\16\7\7\2\2\16\17\7\b\2\2\17\20\5\n\6\2\20\21\7\n\2\2"+
		"\21\22\5\6\4\2\22\23\b\2\1\2\23\3\3\2\2\2\24\25\7\5\2\2\25\26\5\6\4\2"+
		"\26\27\7\6\2\2\27\30\b\3\1\2\30\34\3\2\2\2\31\32\7\7\2\2\32\34\b\3\1\2"+
		"\33\24\3\2\2\2\33\31\3\2\2\2\34\5\3\2\2\2\35\37\5\4\3\2\36\35\3\2\2\2"+
		"\37\"\3\2\2\2 \36\3\2\2\2 !\3\2\2\2!#\3\2\2\2\" \3\2\2\2#-\5\2\2\2$&\5"+
		"\4\3\2%$\3\2\2\2&\'\3\2\2\2\'%\3\2\2\2\'(\3\2\2\2(*\3\2\2\2)+\5\2\2\2"+
		"*)\3\2\2\2*+\3\2\2\2+-\3\2\2\2, \3\2\2\2,%\3\2\2\2-.\3\2\2\2./\b\4\1\2"+
		"/\7\3\2\2\2\60\61\7\7\2\2\618\b\5\1\2\62\63\7\5\2\2\63\64\5\n\6\2\64\65"+
		"\7\6\2\2\65\66\b\5\1\2\668\3\2\2\2\67\60\3\2\2\2\67\62\3\2\2\28\t\3\2"+
		"\2\29:\5\b\5\2:;\b\6\1\2;B\3\2\2\2<=\5\b\5\2=>\7\4\2\2>?\5\n\6\2?@\b\6"+
		"\1\2@B\3\2\2\2A9\3\2\2\2A<\3\2\2\2B\13\3\2\2\2\t\33 \'*,\67A";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}