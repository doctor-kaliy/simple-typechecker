// Generated from /home/eugene/pidor/simple-typechecker/src/main/scala/Lambda.g4 by ANTLR 4.9.2

package gen;
import typechecker.*;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link LambdaParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface LambdaVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link LambdaParser#lambda}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLambda(LambdaParser.LambdaContext ctx);
	/**
	 * Visit a parse tree produced by {@link LambdaParser#pexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPexpr(LambdaParser.PexprContext ctx);
	/**
	 * Visit a parse tree produced by {@link LambdaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(LambdaParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link LambdaParser#ptype}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPtype(LambdaParser.PtypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link LambdaParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(LambdaParser.TypeContext ctx);
}