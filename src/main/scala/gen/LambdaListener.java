// Generated from /home/eugene/pidor/simple-typechecker/src/main/scala/Lambda.g4 by ANTLR 4.9.2

package gen;
import typechecker.*;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link LambdaParser}.
 */
public interface LambdaListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link LambdaParser#lambda}.
	 * @param ctx the parse tree
	 */
	void enterLambda(LambdaParser.LambdaContext ctx);
	/**
	 * Exit a parse tree produced by {@link LambdaParser#lambda}.
	 * @param ctx the parse tree
	 */
	void exitLambda(LambdaParser.LambdaContext ctx);
	/**
	 * Enter a parse tree produced by {@link LambdaParser#pexpr}.
	 * @param ctx the parse tree
	 */
	void enterPexpr(LambdaParser.PexprContext ctx);
	/**
	 * Exit a parse tree produced by {@link LambdaParser#pexpr}.
	 * @param ctx the parse tree
	 */
	void exitPexpr(LambdaParser.PexprContext ctx);
	/**
	 * Enter a parse tree produced by {@link LambdaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(LambdaParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link LambdaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(LambdaParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link LambdaParser#ptype}.
	 * @param ctx the parse tree
	 */
	void enterPtype(LambdaParser.PtypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link LambdaParser#ptype}.
	 * @param ctx the parse tree
	 */
	void exitPtype(LambdaParser.PtypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link LambdaParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(LambdaParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link LambdaParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(LambdaParser.TypeContext ctx);
}