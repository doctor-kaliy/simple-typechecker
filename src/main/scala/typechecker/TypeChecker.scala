package typechecker

import cats.MonadError
import cats.data.ReaderT
import cats.mtl.Local
import cats.mtl.implicits.toReaderOps
import cats.mtl.syntax.local._
import cats.mtl.syntax.ask._
import cats.syntax.applicative._
import cats.syntax.applicativeError._
import cats.syntax.flatMap._
import cats.syntax.functor._

// Типы
sealed trait Type {
  def :->(t: Type): :-> = new :->(this, t)
}

object Type {
  // Удобное название для вызова из ANTLR вместо :->
  def arrow(a: Type, b: Type): Type = a :-> b
}

// Стрелочный тип
case class :->(a: Type, b: Type) extends Type {
  override def toString: String = s"($a -> $b)"
}

// Типовая переменная
case class TVariable(name: String) extends Type {
  override def toString: String = name
}

// Выражения
sealed trait Expression {
  def <*>(e: Expression): Expression = EApplication(this, e)
}

//Переменная
case class EVariable(name: String) extends Expression {
  override def toString: String = name
}

//Лямбда абстракция
case class Abstraction(name: String, t: Type, e: Expression) extends Expression {
  override def toString: String = s"\\$name : $t . $e"
}

//Применение
case class EApplication(a: Expression, b: Expression) extends Expression {
  override def toString: String = s"$a $b"
}

object TypeChecker {
  type Context = Map[String, Type]

  // Проверяет, что можно вывести тип с фиксированным контекстом и возвращает его или сообщает о неудаче
  private def check[F[_]
    : MonadError[*[_], String]
    : Local[*[_], Context]
  ](
    expression: Expression
  ): F[Type] = expression match {

    case EVariable(name) =>
      ((c: Context) => c.get(name)).reader >>= {
        case Some(value: Type) => value.pure[F]
        case None => s"Unexpected name $name".raiseError[F, Type]
      }

    case Abstraction(name, t, e) =>
      for {
        _ <- (identity[Context](_)).reader >>= { (m: Context) =>  // Проверка, что в контексте нет переменной
          if (m.contains(name))
            s"More than one occurrence of variable $name in context is forbidden"
              .raiseError[F, Unit]
          else ().pure
        }
        res <- check[F](e).local((c: Context) => c.updated(name, t)).map(t :-> _)
      } yield res

    case EApplication(a, b) =>
      for {
        t1 <- check(a)
        t2 <- check(b)
        result <- t1 match {
          case t3 :-> resultType if t3 == t2 => resultType.pure
          case _ => s"Term of type $t1 cannot be applied to term of $t2".raiseError
        }
      } yield result
  }

  def run[F[_] : MonadError[*[_], String]](
    expression: Expression,
    context: Context
  ): F[Type] = check[ReaderT[F, Context, *]](expression).run(context)
}
