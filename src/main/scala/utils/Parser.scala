package utils

import gen.{LambdaLexer, LambdaParser}
import org.antlr.v4.runtime.{CharStreams, CommonTokenStream}
import typechecker.{Expression, Type}

// Парсер сгенерирован при помощи ANTLR на джаве и вызван отсюда.
object Parser {
  private def parse(source: String): LambdaParser = {
    val lexer = new LambdaLexer(CharStreams.fromString(source))
    new LambdaParser(new CommonTokenStream(lexer))
  }

  def parseExpression(source: String): Expression = parse(source).expr().exp

  def parseType(source: String): Type = parse(source).`type`().tp
}
