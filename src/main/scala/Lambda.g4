grammar Lambda ;

@header {
package gen;
import typechecker.*;
}

lambda [List<typechecker.Expression> exps] :
    (LAMBDA nm = NAME COLON t = type DOT e = expr {$exps.add(new typechecker.Abstraction($nm.text, $t.tp, $e.exp));}) ;

pexpr [List<typechecker.Expression> exps] :
    (LPAR e = expr RPAR {$exps.add($e.exp);}) |
    (nm = NAME {$exps.add(new EVariable($nm.text));});

expr returns [typechecker.Expression exp]
    @init {
        List<typechecker.Expression> exps = new ArrayList<>();
    } :
    (((pexpr [exps])* lambda [exps]) |
    ((pexpr [exps])+ (lambda [exps])?)) {
        $exp = exps.stream().reduce((x, y) -> new EApplication(x, y)).get();
    } ;

ptype returns [typechecker.Type tp] :
    (n = NAME {$tp = new typechecker.TVariable($n.text);}) |
    (LPAR t = type RPAR {$tp = $t.tp;}) ;

type returns [typechecker.Type tp] :
    (pt = ptype {$tp = $pt.tp;}) |
    (t1 = ptype ARROW t2 = type {$tp = typechecker.Type.arrow($t1.tp, $t2.tp);}) ;

SKIP_WHITESPACES: [ \n\r\t]+ -> skip;
ARROW : '->' ;
LPAR : '(' ;
RPAR : ')' ;
NAME : [a-zA-Z][a-zA-Z0-9_]* ;
COLON : ':' ;
LAMBDA : '\\' ;
DOT : '.' ;
